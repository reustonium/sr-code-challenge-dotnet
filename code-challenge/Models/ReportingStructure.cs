using System.Collections.Generic;

namespace challenge.Models
{
    public class ReportingStructure
    {
        public Employee Employee { get; set; }
        public int NumberOfReports { get; set; }

        public ReportingStructure(Employee employee)
        {
            Employee = employee;
            NumberOfReports = CalculateReports(employee.DirectReports);
        }

        private int CalculateReports(List<Employee> directReports)
        {
            int totalReports = 0;

            if (directReports != null)
            {
                totalReports += directReports.Count;

                foreach (Employee employee in directReports)
                {
                    totalReports += CalculateReports(employee.DirectReports);
                }
            }
            return totalReports;
        }
    }
}
